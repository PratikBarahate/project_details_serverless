package com.dashboard.function;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.dashboard.model.ProjectResponse;
import com.dashboard.service.ProjectService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProjectFunctionTest {
    @Mock
    private ProjectService service;

    @InjectMocks
    private ProjectFunction lambdaFunction;
    @Test
    public void testGetAllProjects() {
        List<ProjectResponse> expectedResponse = new ArrayList<>();
        expectedResponse.add(new ProjectResponse());
        expectedResponse.add(new ProjectResponse());
        when(service.getAllProjects()).thenReturn(expectedResponse);
        APIGatewayProxyResponseEvent responseEvent = lambdaFunction.getAllProjects().get();
        assertEquals(org.apache.http.HttpStatus.SC_OK, responseEvent.getStatusCode());
        assertEquals(expectedResponse.toString(), responseEvent.getBody());
    }
    @Test
    public void testGetProjectById() throws Exception{
        APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();
        requestEvent.setPathParameters(Collections.singletonMap("projectId", "123"));
        ProjectResponse expectedResponse = new ProjectResponse();
        when(service.getProjectById("123")).thenReturn(expectedResponse);
        APIGatewayProxyResponseEvent responseEvent = lambdaFunction.getProjectById().apply(requestEvent);
        assertEquals(org.apache.http.HttpStatus.SC_OK, responseEvent.getStatusCode());
        assertEquals(expectedResponse.toString(), responseEvent.getBody());
    }

    //    @Test
//    public void testAddProjectDetails() {
//        APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();
//        ProjectResponse projectResponse = new ProjectResponse();
//        projectResponse.setProjectName("Test Project");
//        projectResponse.setDescription("Test Description");
//
//
//        Mockito.when(service.saveProjectDetails(projectResponse)).thenReturn(new ProjectResponse(1L, "Test Project", "Test Description"));
//
//        APIGatewayProxyResponseEvent responseEvent = lambdaFunction.addProjectDetails().apply(requestEvent);
//
//        assertEquals(expectedResponseEvent.getStatusCode(), responseEvent.getStatusCode());
//        assertEquals(expectedResponseEvent.getBody(), responseEvent.getBody());
//        verify(service).saveProjectDetails(any(ProjectResponse.class));
//    }



}
