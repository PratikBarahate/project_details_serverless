package com.dashboard.function;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.dashboard.model.ProjectManagerResponse;
import com.dashboard.service.ProjectManagerService;
import com.dashboard.service.ProjectService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ProjectManagerFunctionTest {

    @Mock
    private ProjectManagerService service;

    @InjectMocks
    private ProjectManagerFunction lambdaFunction;

    @Test
    public void testGetProjectManagerDetailsByIdSuccess() {
        APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();
        requestEvent.setPathParameters(Collections.singletonMap("projectId", "123"));
        requestEvent.setHeaders(Collections.singletonMap("Authorization", "Bearer abc123"));
        ProjectManagerResponse expectedResponse = new ProjectManagerResponse();
        Mockito.when(service.getProjectManagerDetailsById(Mockito.eq(123), Mockito.anyString()))
                .thenReturn(expectedResponse);
        APIGatewayProxyResponseEvent responseEvent = lambdaFunction.getProjectManagerDetails().apply(requestEvent);
        assertEquals(200, responseEvent.getStatusCode().intValue());
        assertEquals(expectedResponse.toString(), responseEvent.getBody());
    }
    @Test
    public void testGetAllProjectManagerDetails() throws Exception {
        APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();
        requestEvent.setHeaders(Collections.singletonMap("Authorization", "Bearer abc123"));
        List<ProjectManagerResponse> expectedResponse = new ArrayList<>();
        expectedResponse.add(new ProjectManagerResponse());
        expectedResponse.add(new ProjectManagerResponse());
        Mockito.when(service.getAllProjectManagerDetails(Mockito.anyString())).thenReturn(expectedResponse);
        APIGatewayProxyResponseEvent result = lambdaFunction.getAllProjectManagerDetails().apply(requestEvent);
        //verify(service).getAllProjectManagerDetails("my-auth-token");
        assertEquals(org.apache.http.HttpStatus.SC_OK, result.getStatusCode().intValue());
        assertEquals(expectedResponse.toString(), result.getBody());
    }
}

