package com.dashboard.repository;

import com.dashboard.entity.ProjectEntity;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface ProjectRepository extends CrudRepository<ProjectEntity, String> {
}
