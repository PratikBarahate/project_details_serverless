package com.dashboard.controller;

import com.dashboard.model.ProjectManagerResponse;
import com.dashboard.model.ProjectResponse;
import com.dashboard.service.ProjectService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


//@RestController
//@RequestMapping("/project")
//@Api(tags = "Project Controller", description = "Create and retrieve project")
public class ProjectController {

//    @Autowired
//    ProjectService service;
//
//    @PostMapping("/save")
//    @ApiOperation(value = "Save Project", notes = "Saves project")
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "Record Inserted Successfully")})
//    public ResponseEntity<String> addProjectDetails(@RequestBody ProjectResponse request) {
//        service.saveProjectDetails(request);
//        return new ResponseEntity<String>("Record Inserted Successfully", HttpStatus.OK);
//    }
//
//    @GetMapping("/{projectId}")
//    @ApiOperation(value = "Get project detail for particular project id", notes = "Get project detail for particular project id")
//    @ApiResponses(
//            value = {@ApiResponse(code = 200, message = "OK", response = ProjectResponse.class),
//                    @ApiResponse(code = 404, message = "Data does not exist", response = ResponseStatusException.class)})
//    public ResponseEntity<ProjectResponse> getProjectById(@PathVariable(required = true) Long projectId) throws Exception {
//        ProjectResponse response = service.getProjectById(projectId);
//        return new ResponseEntity<ProjectResponse>(response, HttpStatus.OK);
//    }
//
//    @GetMapping("/getAllProjects")
//    @ApiOperation(value = "Gets All Projects", notes = "Get all list of Projects")
//    @ApiResponses(
//            value = {@ApiResponse(code = 200, message = "OK", response = ProjectResponse.class, responseContainer = "List")})
//    public ResponseEntity<List<ProjectResponse>> getAllProjects() throws Exception {
//        List<ProjectResponse> response = service.getAllProjects();
//        return new ResponseEntity<>(response, HttpStatus.OK);
//    }


}
