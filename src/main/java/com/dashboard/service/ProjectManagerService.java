package com.dashboard.service;

import com.dashboard.entity.ProjectManagerEntity;
import com.dashboard.model.ProjectManagerResponse;
import com.dashboard.model.UserDetails;
import com.dashboard.model.UserResponse;
//import com.dashboard.repository.ProjectManagerRepository;
import com.dashboard.util.RestTemplateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectManagerService {

//    @Autowired
//    ProjectManagerRepository repository;

    @Autowired
    RestTemplateUtil restTemplate;

    public List<ProjectManagerResponse> getAllProjectManagerDetails(String token) {
//        List<ProjectManagerEntity> entity = repository.findAll();
        List<ProjectManagerEntity> entity = new ArrayList<>();
        List<UserResponse> userDetails = restTemplate.getUserDetails(token);
        return mapProjectManagerResponse(entity, userDetails);
    }


    public ProjectManagerResponse getProjectManagerDetailsById(int projectId, String token) {
//        ProjectManagerEntity managerDetails = repository.findByProjectIdId(projectId);
        ProjectManagerEntity managerDetails = new ProjectManagerEntity();
        List<UserResponse> userDetails = restTemplate.getUserDetails(token);
        if (managerDetails != null) {
            ProjectManagerResponse response = new ProjectManagerResponse();
            BeanUtils.copyProperties(managerDetails, response);
            if (managerDetails.getDelivery_lead() != null) {
                response.setDeliveryLead(mapManagerUserDetails(managerDetails.getDelivery_lead(), userDetails));
            }
            if (managerDetails.getOffshore_PM() != null) {
                response.setOffShorePMDetails(mapManagerUserDetails(managerDetails.getOffshore_PM(), userDetails));
            }
            if (managerDetails.getOnsite_PM() != null) {
                response.setOnsitePMDetails(mapManagerUserDetails(managerDetails.getOnsite_PM(), userDetails));
            }
            if (managerDetails.getPortfolio_lead() != null) {
                response.setPortfolioLeadDetails(mapManagerUserDetails(managerDetails.getPortfolio_lead(), userDetails));
            }
            return response;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data does not exist with the projectId " + projectId);
        }

    }

    private List<ProjectManagerResponse> mapProjectManagerResponse(List<ProjectManagerEntity> entity, List<UserResponse> userDetails) {
        List<ProjectManagerResponse> response = new ArrayList<>();
        for (ProjectManagerEntity projectManager : entity) {
            ProjectManagerResponse projectManagerResponse = new ProjectManagerResponse();
            BeanUtils.copyProperties(projectManager, projectManagerResponse);
            if (projectManager.getDelivery_lead() != null) {
                projectManagerResponse.setDeliveryLead(mapManagerUserDetails(projectManager.getDelivery_lead(), userDetails));
            }
            if (projectManager.getOffshore_PM() != null) {
                projectManagerResponse.setOffShorePMDetails(mapManagerUserDetails(projectManager.getOffshore_PM(), userDetails));
            }
            if (projectManager.getOnsite_PM() != null) {
                projectManagerResponse.setOnsitePMDetails(mapManagerUserDetails(projectManager.getOnsite_PM(), userDetails));
            }
            if (projectManager.getPortfolio_lead() != null) {
                projectManagerResponse.setPortfolioLeadDetails(mapManagerUserDetails(projectManager.getPortfolio_lead(), userDetails));
            }
            response.add(projectManagerResponse);
        }
        return response;
    }

    private UserDetails mapManagerUserDetails(String userId, List<UserResponse> userDetails) {
        UserDetails user = null;
        Optional<UserResponse> response = userDetails.stream().filter(s -> s.getUserId() == Long.parseLong(userId)).findAny();
        if (response.isPresent()) {
            user = new UserDetails();
            BeanUtils.copyProperties(response.get(), user);
        }
        return user;
    }
}
