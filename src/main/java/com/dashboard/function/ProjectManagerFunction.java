package com.dashboard.function;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.dashboard.model.ProjectManagerResponse;
import com.dashboard.service.ProjectManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

//@Configuration
@Component
public class ProjectManagerFunction {
    @Autowired
    ProjectManagerService service;

    //@Bean
    public Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> getAllProjectManagerDetails(){
        return (proxyRequestEvent)->{
            APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
            try {
                String authorization = proxyRequestEvent.getHeaders().get("Authorization");
                List<ProjectManagerResponse> response = service.getAllProjectManagerDetails(authorization);
                responseEvent.setBody(response.toString());
                responseEvent.setStatusCode(org.apache.http.HttpStatus.SC_OK);
                return responseEvent;
            }
            catch (Exception e) {
                e.printStackTrace();
                return new APIGatewayProxyResponseEvent().withStatusCode(500);
            }
        };
    }

    //@Bean
    public Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> getProjectManagerDetails(){
        return (proxyRequestEvent) -> {
            APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
            try {
                String authorization = proxyRequestEvent.getHeaders().get("Authorization");
                ProjectManagerResponse response = service.getProjectManagerDetailsById(Integer.parseInt(proxyRequestEvent.getPathParameters().get("projectId")), authorization);
                responseEvent.setStatusCode(org.apache.http.HttpStatus.SC_OK);
                responseEvent.setBody(response.toString());
                return responseEvent;
            } catch (Exception e) {
                e.printStackTrace();
                return new APIGatewayProxyResponseEvent().withStatusCode(500);
            }
        };
    }
}
