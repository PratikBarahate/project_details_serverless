package com.dashboard.function;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.dashboard.model.ProjectResponse;
import com.dashboard.service.ProjectService;
import org.apache.http.HttpStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
public class ProjectFunction {
    @Autowired
    ProjectService service;

    // This method takes input of a projectDetails bean from APIGatewayProxyRequestEvent and saved it into ProjectEntity DynamoDb
    // using that returns a Function which returns APIGatewayProxyResponseEvent with Saved Bean and status code.
    @Bean
    public Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> addProjectDetails(){
        return (value) -> {
            APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
            try {
                ProjectResponse response = new ProjectResponse();
                BeanUtils.copyProperties(value,response);
                service.saveProjectDetails(response);
                responseEvent.setStatusCode(HttpStatus.SC_OK);
                responseEvent.setBody("Record Inserted Successfully");
                return responseEvent;
            } catch (Exception e) {
                e.printStackTrace();
                return new APIGatewayProxyResponseEvent().withStatusCode(500);
            }
        };
    }

    // This method returns a Supplier to handle requests for retrieving all projects
    // and returning an APIGatewayProxyResponseEvent with List of Projects and appropriate Status code
    @Bean
    public Supplier<APIGatewayProxyResponseEvent> getAllProjects(){
        return ()->{
            APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
            try {
                List<ProjectResponse> response = service.getAllProjects();
                responseEvent.setBody(response.toString());
                responseEvent.setStatusCode(HttpStatus.SC_OK);
                return responseEvent;
            }
            catch (Exception e) {
                e.printStackTrace();
                return new APIGatewayProxyResponseEvent().withStatusCode(500);
            }
        };
    }


    // This method creates a bean that returns a Function to handle requests for retrieving a project by ID
    // and returning an APIGatewayProxyResponseEvent.
    @Bean
    public Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> getProjectById(){
        return (proxyRequestEvent) -> {
            APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
            try {
                ProjectResponse response = service.getProjectById(proxyRequestEvent.getPathParameters().get("projectId"));
                responseEvent.setStatusCode(HttpStatus.SC_OK);
                responseEvent.setBody(response.toString());
                return responseEvent;
            } catch (Exception e) {
                e.printStackTrace();
                return new APIGatewayProxyResponseEvent().withStatusCode(500);
            }
        };
    }
}
